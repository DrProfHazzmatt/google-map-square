import { TestBed, ComponentFixture } from '@angular/core/testing';
import { AppComponent } from './app.component';

import { BrowserModule }  from '@angular/platform-browser';
import { FormsModule }    from '@angular/forms';

describe('AppComponent', () => {
	let component: AppComponent;
	let fixture: ComponentFixture<AppComponent>;

	beforeEach(() => {
		TestBed.configureTestingModule({
			declarations: [AppComponent],
			imports: [
				BrowserModule,
				FormsModule
			]
		});

		fixture = TestBed.createComponent(AppComponent);

		component = fixture.componentInstance;
	});

	it('generateSquare() exists.', () => {
		expect(component.generateSquare).toBeDefined();
	});

	it('calculateLatLng() exists.', () => {
		expect(component.calculateLatLng).toBeDefined();
	});

	it('Returns an image string for 39.963689, -75.553421 with an edge distance of 100 miles.', () => {
		component.location = "39.963689, -75.553421";

		component.generateSquare();

		expect(component.calculateLatLng).toHaveBeenCalled();
		expect(component.imageUrl).toBe('https://maps.googleapis.com/maps/api/staticmap?center=39.963689, -75.553421&path=color:blue|weight:5|fillcolor:blue|40.979266,-76.909006|40.979266,-74.197836|38.932788,-74.237814|38.932788,-76.869028|40.979266,-76.909006&zoom=7&size=350x350&key=AIzaSyA5WFEw9nMtWZ4VpB0rCTCezyB3cGFaoFc');
	});
});
