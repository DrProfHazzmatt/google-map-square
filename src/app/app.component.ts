import { Component } from '@angular/core';

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.css']
})
export class AppComponent {
	distanceMilesToKM: number = 1.609344;
	earthRadiusKM: number = 6371;

	location: string;
	splitLocation: string[];
	lng: number = 0;
	latRad: number;
	lngRad: number;
	distance: number = 100;
	distanceKM: number;
	zoomLevel: number = 7;

	topLeft: string[];
	topRight: string[];
	bottomRight: string[];
	bottomLeft: string[];

	imageUrl: string;

	generateSquare(): void
	{
		this.splitLocation = this.location.split(",");

		this.latRad = parseFloat(this.splitLocation[0]) * Math.PI/180;
		this.lngRad = parseFloat(this.splitLocation[1]) * Math.PI/180;
		this.distanceKM = this.distance * this.distanceMilesToKM;

		this.topLeft = this.calculateLatLng(315);
		this.topRight = this.calculateLatLng(45);
		this.bottomRight = this.calculateLatLng(135);
		this.bottomLeft = this.calculateLatLng(225);

		this.imageUrl = "https://maps.googleapis.com/maps/api/staticmap?center=" + this.splitLocation[0] + "," + this.splitLocation[1] + "&path=color:blue|weight:5|fillcolor:blue|" + this.topLeft[0] + "," + this.topLeft[1] + "|" + this.topRight[0] + "," + this.topRight[1] + "|" + this.bottomRight[0] + "," + this.bottomRight[1] + "|" + this.bottomLeft[0] + "," + this.bottomLeft[1] + "|" + this.topLeft[0] + "," + this.topLeft[1] + "&zoom=" + this.zoomLevel + "&size=350x350&key=AIzaSyA5WFEw9nMtWZ4VpB0rCTCezyB3cGFaoFc";
	}

	calculateLatLng(tempBearing: number): string[]
	{
		let bearing: number = tempBearing * Math.PI/180;
		let adjLat: number;
		let adjLng: number;

		adjLat = Math.asin(Math.sin(this.latRad) * Math.cos(this.distanceKM/this.earthRadiusKM) + Math.cos(this.latRad) * Math.sin(this.distanceKM/this.earthRadiusKM) * Math.cos(bearing));
		adjLng = this.lngRad + Math.atan2(Math.sin(bearing) * Math.sin(this.distanceKM/this.earthRadiusKM) * Math.cos(this.latRad), Math.cos(this.distanceKM/this.earthRadiusKM) - Math.sin(this.latRad) * Math.sin(adjLat));

		adjLat = adjLat * 180/Math.PI;
		adjLng = adjLng * 180/Math.PI;

		return [adjLat.toFixed(6), adjLng.toFixed(6)];
	}
}