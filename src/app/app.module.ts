import { BrowserModule }  from '@angular/platform-browser';
import { NgModule }       from '@angular/core';
import { FormsModule }    from '@angular/forms';

//Installed modules
import { NgbModule }		  from '@ng-bootstrap/ng-bootstrap';

//Components
import { AppComponent }   from './app.component';


@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
	BrowserModule,
  NgbModule.forRoot(),
  FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
